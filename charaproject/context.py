from django.shortcuts import render

from app.models import DiscountHead


def discount(request):
    context = {}
    context['discounts'] = DiscountHead.objects.all()
    return context
    # return render(request, 'base.html', {'context': context})

from django.urls import path, include
from . import views
from django.contrib.auth import views as auth_views
from django.utils.translation import gettext_lazy as _

app_name = 'app'

urlpatterns = [
    path('', views.HomeView.as_view(), name='home'),
    path(_('about/'), views.AboutUsView.as_view(), name='about'),
    path(_('new/'), views.NewsView.as_view(), name='new'),
    path(_('women/'), views.WomenView.as_view(), name='women'),
    path(_('girls/'), views.NewsView.as_view(), name='girls'),
    path(_('contact/'), views.ContactView.as_view(), name='contact'),
    path(_('register/'), views.register, name='register'),
    path(_('product_list/'), views.product_list, name='product_list'),
    path('<slug:category_slug>/', views.product_list, name='product_list_by_category'),
    path('product_list/<slug:id>/', views.product_detail, name='product_detail'),

]

from django.shortcuts import render, get_object_or_404
from .forms import MessageForm, SearchForm, UserRegistrationForm
from .models import Product, Contacts, Discount, ProductCategory, AboutUs, BannerState, AllACCESSORIES, ProductTypes
from django.views.generic.base import TemplateView
from django.contrib import messages
from django.contrib.postgres.search import SearchVector, SearchQuery, SearchRank


class HomeView(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['Banner'] = BannerState.objects.all()
        context['Discount'] = Discount.objects.all()
        context['ACCESSORIES'] = AllACCESSORIES.objects.all()
        context['Products'] = Product.objects.all()
        context['ProductsTypes'] = ProductTypes.objects.all()
        return context


class ProductCategoryView(TemplateView):
    template_name = 'base.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['content'] = ProductCategory.objects.first()
        return context


# class ProductList(TemplateView):
#     template_name = 'base.html'


def product_list(request, category_slug=None):
    category = None
    categories = ProductCategory.objects.all()
    products = Product.objects.filter(available=True)
    if category_slug:
        category = get_object_or_404(ProductCategory, slug=category_slug)
        products = products.fiter(category=category)
    return render(request, 'products/list.html', {'category': category, 'categories': categories, 'products': products})


def product_detail(request, id):
    product = get_object_or_404(Product,  slug=id, available=True)
    return render(request, 'products/detail.html', {'product': product})


class ContactView(TemplateView):
    template_name = 'contact.html'

    def get_context_data(self, **kwargs):
        kwargs['contact'] = Contacts.objects.first()
        if 'message_from' not in kwargs:
            kwargs['message_form'] = MessageForm()
        return kwargs

    def post(self, request, *args, **kwargs):
        context = {}
        message_form = MessageForm(data=request.POST)
        if message_form.is_valid():
            message_form.save()
            username = message_form.cleaned_data['name'].capitalize()
            messages.success(request, f'{username}, Your message has been sent successfully.')
        else:
            context['message_form'] = message_form
        return render(request, self.template_name, self.get_context_data(**context))


class AboutUsView(TemplateView):
    template_name = 'about.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['about_us'] = AboutUs.objects.first()
        return context


class NewsView(TemplateView):
    template_name = 'new.html'


class WomenView(TemplateView):
    template_name = 'women.html'


def product_search(request):
    form = SearchForm()
    query = None
    results = []
    if 'query' in request.POST:
        form = SearchForm(request.POST)
        if form.is_valid():
            query = form.cleaned_data['query']
            search_vector = SearchVector('title', weight='A') + SearchVector('price', weight='B')
            search_query = SearchQuery(query)
            results = Product.price.annotate(
                search=search_vector,
                rank=SearchRank(search_vector, search_query)
            ).filter(rank__gte=0.3).order_by('-rank')
    return render(request, 'base.html', {'form': form, 'query': query, 'results': results})


def register(request):
    if request.method == 'POST':
        user_form = UserRegistrationForm(request.POST)
        if user_form.is_valid():
            # create a new user object but avoid saving it yet
            new_user = user_form.save(commit=False)
            # set the chosen password
            new_user.set_password(user_form.cleaned_data['password'])
            # save the User object
            new_user.save()
            return render(request, 'account/register_done.html', {'new_user': new_user})
    else:
        user_form = UserRegistrationForm()
    return render(request, 'account/register.html', {'user_form': user_form})


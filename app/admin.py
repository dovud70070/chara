from django.contrib import admin
from .models import Product, ProductCategory, Discount, AboutUs, Message, BannerState, AllACCESSORIES, ProductTypes, DiscountHead
from parler.admin import TranslatableAdmin


@admin.register(Product)
class ProductAdmin(TranslatableAdmin):
    list_display = ('name', 'description', 'price', 'category', 'available', 'added', 'updated', 'slug')
    search_fields = ('name', 'description',)
    list_editable = ('price', 'available')
    ordering = ('-added',)
    date_hierarchy = 'added'

    def get_prepopulated_fields(self, request, obj=None):
        return {
            'slug': ('name', )
        }


class ProductInline(admin.TabularInline):
    model = Product


@admin.register(ProductCategory)
class ProductCategoryAdmin(TranslatableAdmin):
    list_display = ('name', 'description')

    def get_prepopulated_fields(self, request, obj=None):
        return {'slug': ('name',)}


admin.site.register(Discount, TranslatableAdmin)
admin.site.register(AboutUs, TranslatableAdmin)
admin.site.register(Message, TranslatableAdmin)
admin.site.register(BannerState, TranslatableAdmin)
admin.site.register(AllACCESSORIES, TranslatableAdmin)
admin.site.register(ProductTypes, TranslatableAdmin)
admin.site.register(DiscountHead, TranslatableAdmin)



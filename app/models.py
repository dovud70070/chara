from django.db import models
from django.utils.text import gettext_lazy as _
from parler.models import TranslatableModel, TranslatedFields
from django.utils.text import slugify
from django.urls import reverse
from parler.tests.utils import User


class Contacts(TranslatableModel):
    translation = TranslatedFields(
        name=models.CharField(max_length=200),
        phoneNumber=models.CharField(max_length=15)
    )
    email = models.EmailField()
    message = models.TextField()

    def __str__(self):
        return self.safe_translation_getter('name')


class Discount(TranslatableModel):
    translations = TranslatedFields(
        sale=models.CharField(max_length=70, verbose_name=_('Discount Name')),
        title=models.CharField(max_length=150, verbose_name=_('Discount for what / percent'))
    )

    def __str__(self):
        return self.safe_translation_getter('sale')


class DiscountHead(TranslatableModel):
    translations = TranslatedFields(
        title=models.CharField(max_length=150, verbose_name=_('Discount for top items'))
    )

    def __str__(self):
        return self.safe_translation_getter('title')


class ProductCategory(TranslatableModel):
    translations = TranslatedFields(
        name=models.CharField(max_length=200, verbose_name=_('Title')),
        description=models.TextField(null=True, verbose_name=_('Description')),

    )
    slug = models.SlugField(max_length=200, unique=True, null=True)

    def __str__(self):
        return self.safe_translation_getter('name')

    def get_absolute_url(self):
        return reverse('app:product_list_by_category', args=[self.slug])


class Product(TranslatableModel):
    translations = TranslatedFields(
        name=models.CharField(max_length=200, verbose_name=_('Product Name'), db_index=True),
        description=models.TextField(verbose_name=_('Product Description')),
    )
    slug=models.SlugField(db_index=True, null=True)

    image = models.ImageField(upload_to='products/%Y/%m/%d',
                              blank=True)
    added = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    available = models.BooleanField(default=True, verbose_name=_('Available on the store'))
    category = models.ForeignKey(ProductCategory, related_name='products',
                                 verbose_name=_('To which category it is related'), on_delete=models.CASCADE)
    price = models.FloatField(verbose_name=_('Price'))
    discount = models.ForeignKey(Discount, related_name='products', verbose_name=_('Product Discount'),
                                 on_delete=models.SET_DEFAULT, default=None, null=True)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super(Product, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('app:product_detail', args=[ self.slug])


class AboutUs(TranslatableModel):
    translation = TranslatedFields(
        title=models.CharField(max_length=200),
        description=models.TextField(max_length=200)
    )
    image = models.ImageField(upload_to='media/images/%Y/%m/%d')

    def __str__(self):
        return self.safe_translation_getter('title') or ''


class Message(TranslatableModel):
    translation = TranslatedFields(
        name=models.CharField(max_length=70),
    )
    phoneNumber = models.CharField(max_length=70, null=True)
    email = models.EmailField()

    def __str__(self):
        return self.safe_translation_getter('name')


class BannerState(TranslatableModel):
    translation = TranslatedFields(
        title = models.CharField(max_length=200),
        title_1 = models.CharField(max_length=200),
    )
    image = models.ImageField(upload_to='products/%Y/%m/%d', blank=True)
    image_1 = models.ImageField(upload_to='products/%Y/%m/%d', blank=True)

    def __str__(self):
        return self.title


class AllACCESSORIES(TranslatableModel):
    translation = TranslatedFields(
        name=models.CharField(max_length=100),
        title=models.CharField(max_length=150),
    )
    image = models.ImageField(upload_to='products/%Y%m/%d', blank=True)

    def __str__(self):
        return self.title


class ProductTypes(TranslatableModel):
    translation = TranslatedFields(
        title=models.CharField(max_length=100),
        description=models.CharField(max_length=200)
    )
    image = models.ImageField(upload_to=True, null=True)

    def __str__(self):
        return self.safe_translation_getter('title')

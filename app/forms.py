from django import forms
from django.utils.translation import ugettext_lazy as _
from .models import Message
from parler.forms import TranslatableModelForm
from django.contrib.auth.models import User


class MessageForm(TranslatableModelForm):

    class Meta:
        model = Message
        fields = ('name', 'phoneNumber', 'email', )
        widgets = {
            'name': forms.TextInput({'placeholder': _('Name')}),
            'phoneNumber': forms.NumberInput({'placeholder': _('Phone Number')}),
            'email': forms.EmailInput({'placeholder': _('Email')}),
            'message': forms.Textarea({'placeholder': _('Your message')})
        }


class SearchForm(forms.Form):
    query = forms.CharField()


class LoginForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)


class UserRegistrationForm(forms.ModelForm):
    password = forms.CharField(label='Password', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Repeat password', widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ('username', 'first_name', 'email')

    def clean_password2(self):
        cd = self.cleaned_data
        if cd['password'] != cd['password2']:
            raise forms.ValidationError('password don\'t match.')
        return cd['password2']


PRODUCT_QUANTITY_CHOICES = [(i, str(i)) for i in range(1, 21)]
